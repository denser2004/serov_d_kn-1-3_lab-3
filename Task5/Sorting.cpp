using namespace std;

void sort_mat(int *a, int na,int ma)
{
	int j,i,k, tmp;
	
	for(i = 1; i < na; i=i+2)
	{
		for(k = 1; k < na; ++k)
		{	
			for(j = 0; j < ma-k; j++)
			{
				if(*(a + i*ma + j) > *(a + i*ma + j+1))
				{
					tmp = *(a + i*ma + j);
					*(a + i*ma + j) = *(a + i*ma + j+1);
					*(a + i*ma + j+1) = tmp;
				}
			}
		}
	}
}
